{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import Control.Monad
import Control.Monad.IO.Class
import Data.Aeson (FromJSON, ToJSON)
import Data.Text
import Data.Version (showVersion)
import GHC.Generics
import Network.HTTP.Types.Status
import Paths_auth_service_lib (version)
import Service.Configuration
import Service.Database
import Service.RunState
import Web.Scotty
import Web.Scotty.TLS

data ServiceHealth = ServiceHealth {
  health :: String
  , serviceVersion :: String
  } deriving (Show, Generic)

instance ToJSON ServiceHealth


main :: IO ()
main = do
  runstate <- createRunState
  _ <- checkInitialState runstate
  scottyTLS
    3000
    (sslKeyFile (getConfig runstate))
    (sslCertFile (getConfig runstate)) $ do
    get "/health" $ do
      let status = ServiceHealth {
            health = "OK"
            , serviceVersion = showVersion version
            }
      Web.Scotty.json status
    post "/key/:owner" $ do
      owner <- param "owner" 
      keyPair <- liftIO $ createNewKeyPair runstate owner
      Web.Scotty.json keyPair
    get "/key/:owner" $ do
      owner <- param "owner"
      liftIO $ putStrLn $ "/key:" ++ (show owner)
      keyPair' <- liftIO $ retrieveLastKeyPair runstate owner
      case keyPair' of
        Nothing      -> status status404
        Just keyPair -> json keyPair
