{-# LANGUAGE OverloadedStrings, DeriveGeneric #-}

{-|
Module      : Service.Database
Description : Manage the key datastore
Copyright   : (c) Benguela, 2019
                  Tim Whelan, 2019
License     : GPL-3
Maintainer  : tim@zipt.co
Stability   : experimental
Portability : POSIX

Manages the key datastore access, including connections (via a Data.Pool)
-}

module Service.Database
  (
    -- * Classes
    KeyPair(..)
    
    -- * Key manipulation functions
  , retrieveAdminKeyPair
  , retrieveLastKeyPair
  , createNewKeyPair
    
    -- * Helper functions
  , checkInitialState
  )
  where

import Benguela.Auth.RSA
import Data.Aeson (FromJSON, ToJSON)
import Data.Pool
import Data.Text
import Data.Default
import Data.Time
import Database.PostgreSQL.Simple
import Database.PostgreSQL.Simple.FromRow
import Database.PostgreSQL.Simple.ToRow
import Database.PostgreSQL.Simple.ToField
import GHC.Generics
import Service.Configuration
import Service.RunState

instance Default UTCTime where
  def = read "1970-01-01 00:00:00" :: UTCTime
  
instance Default LocalTime where
  def = read "1970-01-01 00:00:00" :: LocalTime
  
data KeyPair = KeyPair {
  keyPairId :: Integer
  , dateCreated :: LocalTime
  , originatingIP :: Maybe Text
  , owner :: Text
  , keyVersion :: Integer
  , certificate :: Text
  , privateKey :: Text
  , keyFlags :: Text
  } deriving (Show, Generic)

instance ToJSON KeyPair
    
instance FromRow KeyPair where
  fromRow = KeyPair <$> field -- id
            <*> field         -- dateCreated
            <*> field         -- originatingIP
            <*> field         -- owner
            <*> field         -- keyVersion
            <*> field         -- certificate
            <*> field         -- privateKey
            <*> field         -- keyFlags

instance ToRow KeyPair where
  toRow kp = [toField (keyPairId kp)
             , toField (dateCreated kp)
             , toField (originatingIP kp)
             , toField (owner kp)
             , toField (keyVersion kp)
             , toField (certificate kp)
             , toField (privateKey kp)
             , toField (keyFlags kp)]
    
-- | Retrieve the administrative key pair
retrieveAdminKeyPair :: RunState -> IO (Maybe KeyPair)
retrieveAdminKeyPair runstate = do
  admin' <- withResource (getConnectionPool runstate)
    (\conn -> (query_ conn "SELECT * FROM keys WHERE keyFlags = 'ADMIN'") :: IO [KeyPair])
  putStrLn $ "Found" ++ show (Prelude.length admin') ++ " admin keys"
  return $ case Prelude.length admin' of
    0 -> Nothing
    _ -> Just (Prelude.head admin')

-- | Check the start up state of the datastore, and initialise it if
-- necessary.  Currently checks that there is an admin key present and
-- errors if not.
checkInitialState :: RunState -> IO Bool
checkInitialState runState = do
  adminKey <- retrieveAdminKeyPair runState
  case adminKey of
    Nothing -> do
      putStrLn "Error: can't retrieve admin key"
      return False
    Just _ -> return True

-- | Retrieve most recent key for a given client/owner
retrieveLastKeyPair :: RunState -> Text -> IO (Maybe KeyPair)
retrieveLastKeyPair runstate owner = do
  keys' <- withResource (getConnectionPool runstate)
    (\conn -> (query conn "SELECT * FROM keys WHERE owner = ? ORDER BY keyVersion DESC LIMIT 1"  [owner]))
  return $ case Prelude.length keys' of
    0 -> Nothing
    _ -> Just $ Prelude.head keys'
  
-- | Create a new key pair.
-- This performs now authorisation checking
createNewKeyPair :: RunState -> String -> IO KeyPair
createNewKeyPair runState owner = do
  pair <- generateNewKeyPair pairBitSize
  let keyPair = mkKeyPair pair
  newId <- saveKeyPair runState keyPair
  return $ keyPair { keyPairId = newId }

mkKeyPair :: (PublicKey, PrivateKey) -> KeyPair
mkKeyPair (public, private) = KeyPair {
  keyPairId = 0
  , dateCreated = def
  , originatingIP = Just ""
  , owner = ""
  , keyVersion = 0
  , keyFlags = ""
  , certificate = fromPublicKey public
  , privateKey = fromPrivateKey private
  }

-- | Save the supplied key pair
saveKeyPair :: RunState         -- ^ Run state to use
            -> KeyPair          -- ^ KeyPair to save
            -> IO Integer       -- ^ Id of key pair
saveKeyPair runState keyPair = do
  existing' <- retrieveLastKeyPair runState (owner keyPair)
  let newVersion = case existing' of
        Nothing -> 1
        Just existing -> 1 + keyVersion existing
  rowCount <- withResource (getConnectionPool runState)
    (\conn -> execute conn "INSERT INTO keys(date_created, owner, key_version, certificate, private_key, key_flags) VALUES(now(), ?, ?, ?, ?, '')" keyPair)
  key' <- retrieveLastKeyPair runState (owner keyPair)
  return $
    if rowCount == 0
    then 0
    else do
      case key' of
        Nothing -> 0
        Just key'' -> (keyPairId key'')
    
