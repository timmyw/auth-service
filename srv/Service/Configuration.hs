{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}

module Service.Configuration
  (
    Configuration(..)
  , createConfiguration
  )
  where

import qualified Data.Configurator as DC
import qualified Data.Configurator.Types as DCT
import           Data.Text
import           Service.Service

data Configuration = Configuration {
  databaseConnection :: String
  , sslKeyFile :: String
  , sslCertFile :: String
  } deriving Show

class ConfigValue a where
  getConfigValue :: DCT.Config -> Text -> a -> IO a

instance ConfigValue Text where
  getConfigValue config name defaultValue = do
    v <- DC.lookup config name :: IO (Maybe Text)
    return (case v of
                  Just v' -> v'
                  Nothing -> defaultValue)

instance ConfigValue String where
  getConfigValue config name defaultValue = do
    v <- DC.lookup config name :: IO (Maybe String)
    return (case v of
                  Just v' -> v'
                  Nothing -> defaultValue)

makeConfigFilePath :: String -> String
makeConfigFilePath path = configPath ++ "/" ++ path

createConfiguration :: IO Configuration
createConfiguration = do
  config <- DC.load [ DC.Required configFile ]
  databaseConnection' <- getConfigValue config "database.postgresql.connection" "localhost:5432"
  sslKeyFile' <- getConfigValue config "ssl.key" "/etc/auth-service/key.pem"
  sslCertFile' <- getConfigValue config "ssl.cert" "/etc/auth-service/cert.pem"
  return $ Configuration {
    databaseConnection = databaseConnection'
    , sslKeyFile = makeConfigFilePath sslKeyFile'
    , sslCertFile = makeConfigFilePath sslCertFile'
    }
