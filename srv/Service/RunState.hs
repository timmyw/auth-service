{-# LANGUAGE OverloadedStrings #-}

{-|
Module      : Service.RunState
Description : Data type to store/init run state
Copyright   : (c) Benguela, 2019
                  Tim Whelan, 2019
License     : GPL-3
Maintainer  : tim@zipt.co
Stability   : experimental
Portability : POSIX

Data type to store an initial (and possibly ongoing runstate)
-}

module Service.RunState
  (
    -- * Types
    RunState(..)

    -- * Functions
  , createRunState
  )
  where

import qualified Data.ByteString.Char8 as DBC8
import           Data.Pool
import           Database.PostgreSQL.Simple
import           Service.Configuration

data RunState = RunState {
  getConfig         :: Configuration
  , getConnectionPool :: Pool Connection
  }

-- | Create a datastore connection pool
createConnectionPool :: Configuration -> IO (Pool Connection)
createConnectionPool config = do
  -- conn <- connectPostgreSQL connStr
  -- [Only i] <- (query_ conn "select count(*) from keys") :: IO [Only Integer]
  -- print i
  createPool (connectPostgreSQL connStr) (close) 1 10 5
             where connStr = DBC8.pack $ databaseConnection config

-- | Create a new RunState and initialise it
createRunState :: IO RunState   -- ^ The inititalised RunState
createRunState  = do
  config <- createConfiguration
  pool <- createConnectionPool config
  let runState = RunState {
        getConfig = config
        , getConnectionPool = pool
        }
  return runState
  
