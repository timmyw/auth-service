{-# LANGUAGE OverloadedStrings #-}

module Service.Service
  (
    serviceName
  , configPath
  , configFile
  )
  where

serviceName :: String
serviceName = "auth-service"

configPath :: String
configPath = "/etc/benguela/auth-service"

configFile :: String
configFile = configPath ++ "/" ++ serviceName ++ ".config"
