#!/usr/bin/python

import json
import sys

conf_file = sys.argv[1]
key = sys.argv[2]
value = sys.argv[3]
config = json.loads(open(sys.argv[1]).read())
config[key] = value
with open(sys.argv[1], 'w') as fp:
    json.dump(config, fp)
