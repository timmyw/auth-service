#!/usr/bin/perl

my ($name) = @ARGV;

while (my $line = <STDIN>) {
    chomp($line);
    my ($k, $v) = split /=/, $line;
    if ($name eq $k) {
	print $v;
    }
}
