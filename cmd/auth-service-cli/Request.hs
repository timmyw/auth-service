{-# LANGUAGE OverloadedStrings #-}

{-|
Module      : Request
Description : Send and handle response of GET/POST requests
Copyright   : (c) Benguela, 2019
                  Tim Whelan, 2019
License     : GPL-3
Maintainer  : tim@zipt.co
Stability   : experimental
Portability : POSIX

Sends GET and POST requests and handles responses, errors etc
-}

module Request
  (
    -- * Types
    RequestParameters(..)

    -- * Request functions
  , doHealth
  )
where

import           Control.Exception
import           Control.Lens
import           Control.Monad.IO.Class
import           Data.Aeson
import qualified Data.ByteString.Lazy as DBS
import           Data.Text
import           Data.Text.Lazy
import           Data.Text.Lazy.Encoding
import           Network.Connection
import           Network.HTTP.Client
import           Network.HTTP.Client.TLS
import           Network.HTTP.Types
import           Network.Wreq

data RequestParameters = RequestParameters {
  base :: String
  , validateServerCert :: Bool
  }

tlsSettings :: RequestParameters -> TLSSettings
tlsSettings reqParms = TLSSettingsSimple {
        settingDisableCertificateValidation = (validateServerCert reqParms)
        , settingDisableSession = False
        , settingUseServerName = False
        }

requestOpts :: RequestParameters -> Network.Wreq.Options
requestOpts reqParms = defaults & manager  .~ Left (mkManagerSettings (tlsSettings reqParms) Nothing)

-- | Send a POST Request
-- The FormParam parameter is of the form ["client" := ("defclient1" :: String)]
doRequestPost :: String -> RequestParameters -> FormParam -> IO (Either HttpException (Status, Data.Text.Text))
doRequestPost url reqParms params = do
  r' <- try (postWith (requestOpts reqParms) url params) :: IO (Either HttpException (Response DBS.ByteString))
  case r' of
    Left exception -> return $ Left exception
    Right r -> return $ Right (r ^. Network.Wreq.responseStatus, (toStrict . decodeUtf8) $ r ^. Network.Wreq.responseBody)
  
doRequestGet :: String -> RequestParameters -> IO (Either HttpException (Status, Data.Text.Text))
doRequestGet url reqParms = do
  let tlsSettings = TLSSettingsSimple {
        settingDisableCertificateValidation = (validateServerCert reqParms)
        , settingDisableSession = False
        , settingUseServerName = False
        }
  let opts = defaults & manager  .~ Left (mkManagerSettings tlsSettings Nothing)
  r' <- try (getWith opts url) :: IO (Either HttpException (Response DBS.ByteString))
  case r' of
    Left exception -> return $ Left exception
    Right r -> return $ Right (r ^. Network.Wreq.responseStatus, (toStrict . decodeUtf8) $ r ^. Network.Wreq.responseBody)

doHealth :: RequestParameters -> IO ()
doHealth reqParms = do
  response <- doRequestGet ((base reqParms) ++ "/health") reqParms
  case response of
    Left exception -> do
      putStrLn $  "FAIL:" ++ (show exception)
    Right (status, body) -> do
      if not $ statusIsSuccessful status
        then putStrLn $ "FAIL:" ++ (show status)
        else
        do
          putStrLn $ show body

