{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE OverloadedStrings #-}

module Main
  where

import Data.Version (showVersion)
import Paths_auth_service_lib (version)
import System.Console.CmdArgs
import Request

appName :: String
appName = "auth-service-cli"

data CommandOptions = CommandOptions {
  baseUrl :: String
  , action :: String
  , validateCert :: Bool
  } deriving (Show, Data, Typeable)

cmdOptions :: CommandOptions
cmdOptions = CommandOptions {
  baseUrl = "https://localhost:3000/" &= help "Base URL for service" &= typ "URL"
  , action = def &= help "Action to perform" &= typ "ACTION"
  , validateCert = True &= help "Validate server certificate. Default True" &= typ "BOOL"
  } &= summary (appName ++ " " ++ showVersion version)


main :: IO ()
main = do
  opts <- cmdArgs cmdOptions
  let reqParms = RequestParameters { base = baseUrl opts, validateServerCert = validateCert opts }
  case (action opts) of
    "health" -> doHealth reqParms
