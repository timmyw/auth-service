module Main
  where

import Benguela.Auth.RSA

main :: IO ()
main = do
  (public, private) <- generateNewKeyPair 2048
  print public
  print private
