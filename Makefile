
# 
# Makefile to handle the encryption configuration, extract creds and
# install the configuration.
#

.PHONY: create_cert dec_config enc_config install setup_database install_secrets

config/configuration.plaintext.json: config/configuration.json.enc
	openssl aes-256-cbc -d -a -salt -in $< -out $@

# Create the SSL certificate and key, then inject the cert and key
# from the config directory into the configuration file, which is then
# encrypted.  If the files already exist, the existing certificate and key are used
create_cert:
	test -f config/key.pem || openssl req -x509 -newkey rsa:4096 -keyout config/key.pem -out config/cert.pem -days 365 -nodes
	cp config/configuration.plaintext.json config/configuration.plaintext.json.bak
	cat config/key.pem | tr '\n' '|' | sed -e 's/|/\n/g' > /tmp/oneline-key.pem
	cat config/cert.pem | tr '\n' '|' | sed -e 's/|/\n/g' > /tmp/oneline-cert.pem
	python config/setvalue.py config/configuration.plaintext.json date_generated `date '+%Y%m%d%H%M%S'`
	python config/setvalue.py config/configuration.plaintext.json admin_cert "`cat /tmp/oneline-cert.pem`"
	python config/setvalue.py config/configuration.plaintext.json admin_private_key "`cat /tmp/oneline-key.pem`"
	rm /tmp/oneline-cert.pem /tmp/oneline-key.pem
	openssl aes-256-cbc -a -salt -in config/configuration.plaintext.json -out config/configuration.json.enc

dec_config: config/configuration.json.enc
	openssl aes-256-cbc -d -a -salt -in config/configuration.json.enc -out config/configuration.plaintext.json

enc_config: config/configuration.json
	openssl aes-256-cbc -a -salt -in $< -out config/configuration.json.enc

install: config/configuration.plaintext.json install_secrets 
	mkdir -p /etc/benguela/auth-service
	python config/getvalue.py config/configuration.plaintext.json admin_cert > /etc/benguela/auth-service/cert.pem
	python config/getvalue.py config/configuration.plaintext.json admin_private_key > /etc/benguela/auth-service/key.pem
	mv config/auth-service.secrets.config /etc/benguela/auth-service/auth-service.config
	chown -R ${SUDO_USER} /etc/benguela/auth-service

setup_database: sql/00001-create-database.secrets.sql sql/00002-create-initial-tables.sql sql/runsqlscripts.secrets.sh install_secrets
	sudo -u postgres psql -f sql/00001-create-database.secrets.sql
	$(SHELL) sql/runsqlscripts.secrets.sh

install_secrets: config/configuration.plaintext.json
	sed -e "s/DBPASSWORD/`python config/getvalue.py config/configuration.plaintext.json database_password`/" config/auth-service.config > config/auth-service.secrets.config
	sed -e "s/DBPASSWORD/`python config/getvalue.py config/configuration.plaintext.json database_password`/" sql/00001-create-database.sql > sql/00001-create-database.secrets.sql
	sed -e "s/DBPASSWORD/`python config/getvalue.py config/configuration.plaintext.json database_password`/" sql/runsqlscripts.sh > sql/runsqlscripts.secrets.sh
#	sed -e "s/ADMIN_CERTIFICATE/`python config/getvalue.py config/configuration.plaintext.json admin_cert`/" sql/runsqlscripts.sh > sql/runsqlscripts.secrets.sh
