{-|
Module      : Benguela.Auth.Auth
Description : Manage the generation and storage of keys
Copyright   : (c) Benguela, 2019
                  Tim Whelan, 2019
License     : GPL-3
Maintainer  : tim@zipt.co
Stability   : experimental
Portability : POSIX

Wraps the key generation, handles initialisation and storage.
-}

module Benguela.Auth.Auth
  (
    -- * Functions
  )
  where

