{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}

{-|
Module      : Benguela.Auth.RSA
Description : Create RSA private/public key pairs
Copyright   : (c) Benguela, 2019
                  Tim Whelan, 2019
License     : GPL-3
Maintainer  : tim@zipt.co
Stability   : experimental
Portability : POSIX

Create new RSA keys
-}

module Benguela.Auth.RSA
  (
    -- * Functions
    generateNewKeyPair

    --- * Conversion functions
  , toPrivateKey
  , toPublicKey 

  , fromPrivateKey
  , fromPublicKey

    -- * Constants
  , pairBitSize
  
    -- * Re-exported types
  , PublicKey(..)
  , PrivateKey(..)
  )
  where

import Codec.Crypto.RSA
import Crypto.Random
import Data.Text
import System.Random
import Data.String

pairBitSize :: Int
pairBitSize = 4096

-- | Generate a new public/private key pair.  This is currently a very
-- simple wrapper around generateKeyPair.
generateNewKeyPair ::
  Int                           -- ^ Bit size
  -> IO (PublicKey, PrivateKey) -- ^ New keys
generateNewKeyPair bitSize = do
  g <- newGenIO :: IO SystemRandom
  let (public, private, g') = generateKeyPair g bitSize
  return (public, private)

class (IsString a) => ToPrivateKey a where
  toPrivateKey :: a -> PrivateKey
  
instance ToPrivateKey String where
  toPrivateKey t = (read t) :: PrivateKey

instance ToPrivateKey Text where
  toPrivateKey t = (read $ unpack t) :: PrivateKey

class (IsString a) => ToPublicKey a where
  toPublicKey :: a -> PublicKey

instance ToPublicKey String where
  toPublicKey t = (read t) :: PublicKey

instance ToPublicKey Text where
  toPublicKey t = (read $ unpack t) :: PublicKey

class (IsString a) => FromPrivateKey a where
  fromPrivateKey :: PrivateKey -> a

instance FromPrivateKey Text where
  fromPrivateKey private = pack $ show private

instance FromPrivateKey String where
  fromPrivateKey private = show private

class (IsString a) => FromPublicKey a where
  fromPublicKey :: PublicKey -> a

instance FromPublicKey Text where
  fromPublicKey private = pack $ show private

instance FromPublicKey String where
  fromPublicKey private = show private
