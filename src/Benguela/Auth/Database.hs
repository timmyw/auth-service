{-|
Module      : Benguela.Auth.Database
Description : Persistent storage of keys
Copyright   : (c) Benguela, 2019
                  Tim Whelan, 2019
License     : GPL-3
Maintainer  : tim@zipt.co
Stability   : experimental
Portability : POSIX

Store and manage keys
-}

module Benguela.Auth.Database

  where

-- import Database.PostgreSQL.Simple

