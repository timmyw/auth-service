#!/bin/sh

PGPASSWORD=DBPASSWORD psql --host=localhost -U auth_user auth -f sql/00002-create-initial-tables.sql

CERTIFICATE=`python config/getvalue.py config/configuration.plaintext.json admin_cert`

PRIVATEKEY=`python config/getvalue.py config/configuration.plaintext.json admin_private_key`

PGPASSWORD=CHANGEME psql --host=localhost -U auth_user auth -c "INSERT INTO keys(keyversion, owner, privatekey, certificate, keyflags, datecreated) VALUES (1, 'auth-service', '$PRIVATEKEY', '$CERTIFICATE', 'ADMIN', now())"
