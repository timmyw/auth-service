CREATE SEQUENCE IF NOT EXISTS keys_seq 
       START WITH 1
       INCREMENT BY 1
       NO MINVALUE
       NO MAXVALUE
       ;
       
CREATE TABLE IF NOT EXISTS keys
(
	id INTEGER DEFAULT nextval('keys_seq'::regclass) NOT NULL,
	datecreated TIMESTAMP WITHOUT TIME ZONE,
	originatingip VARCHAR(255),
	owner VARCHAR(255),
	keyversion INTEGER,
	certificate VARCHAR(10240),
	privatekey VARCHAR(10240),
	keyflags VARCHAR(128)
);
